/* Problem from Data Structures & Algorithms in Java Second Edition by Robert Lafore
 * Chapter 2 Programming Project 2.5
 * Create a merge() method that merges two ordered arrays into an ordered
 * destination array.
 */

class OrdArray {
	private long[] a; // reference to array
	private int nElems; // number of data items in array

	public OrdArray(int max) // constructor
	{
		a = new long[max]; // create array
		nElems = 0;
	}

	public int size() {
		return nElems;
	}

	public void insert(long value) // add element into an array in ascending order
						
	{
		int j;
		for (j = 0; j < nElems; j++)
			if (a[j] > value)
				break;
		for (int k = nElems; k > j; k--)
			a[k] = a[k - 1];
		a[j] = value;
		nElems++;
	}

	public long getElem(int x) // retrieve element from array

	{

		return a[x];

	}

	public static OrdArray merge(OrdArray a, OrdArray b) //merge two ordered arrays
	{

		OrdArray answer = new OrdArray(a.size() + b.size());

		int i = 0, j = 0;

		while (i < a.size() && j < b.size()) {
			if (a.getElem(i) < b.getElem(j)) {
				answer.insert(a.getElem(i));
				++i;

			} else {
				answer.insert(b.getElem(j));
				++j;
			}
		}
		while (i < a.size()) {
			answer.insert(a.getElem(i));
			++i;
		}
		while (j < b.size()) {
			answer.insert(b.getElem(j));
			++j;
		}
		return answer;
	}

	public void display() //output array to console
	{
		for (int j = 0; j < nElems; j++)
			System.out.print(a[j] + " ");
		System.out.println("");
	}

}

class OrderedApp {
	public static void main(String[] args) {
		int maxSize = 10; // array size
		OrdArray arr = new OrdArray(maxSize); // create first array

		arr.insert(77); //add 10 items
		arr.insert(99);
		arr.insert(44);
		arr.insert(55);
		arr.insert(22);
		arr.insert(88);
		arr.insert(11);
		arr.insert(00);
		arr.insert(66);
		arr.insert(33);

		OrdArray abb = new OrdArray(maxSize); //create second array
		
		abb.insert(12); // add 10 items
		abb.insert(15);
		abb.insert(18);
		abb.insert(95);
		abb.insert(05);
		abb.insert(65);
		abb.insert(32);
		abb.insert(19);
		abb.insert(20);
		abb.insert(77);

		arr.display(); // display items in first array
		
		abb.display(); // display items in second array

		OrdArray w = OrdArray.merge(abb, arr); // merge ordered arrays

		w.display(); // output merged ordered array

	}

}