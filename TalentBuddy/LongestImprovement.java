
/*
 * Talent Buddy - Longest Improvement
 * Write a function that computes and prints 
 * the longest period of increasing performance for 
 * the students grades.
 */

public class LongestImprovement {
	public static void longest_improvement(Integer[] grades) {
		int count = 0;
		int count_max = 0;
		for (int i = 0; i < grades.length - 1; i++) {
			if (grades[i] <= grades[i + 1]) {
				count = 1 + count;
				if (count > count_max) {
					count_max = count;
				}
			} else {
				count = 1;
			}
		}
		System.out.println(count_max);
	}

	public static void main(String[] args) {
		Integer[] c = { 9, 7, 8, 2, 5, 5, 8, 7 };
		LongestImprovement.longest_improvement(c);
	}
}
