
/*
 * Talent Buddy - Missing Number
 * Given an array containing all numbers from 1 to N 
 * with the exception of one print the missing number.
 */
 
import java.util.Arrays;
 
class MissingNumber {

	public static void find_missing_number(Integer[] v) {
		Arrays.sort(v);

		for (int i = 0; i < v.length; i++) {
			if (v[i] == (v[i+1] - 1)) {
				continue;
			} else {
				System.out.println(v[i] + 1);
				break;
			}
		}
	}

	public static void main(String[] args) {
		Integer[] c = { 5, 4, 1, 2 };
		MissingNumber.find_missing_number(c);
	}

}